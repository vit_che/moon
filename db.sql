
CREATE DATABASE IF NOT EXISTS `aura`;
USE `aura`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image_path` varchar(255) DEFAULT '/images/default.png',
  `file_name` varchar(255) DEFAULT 'default.png',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
	(1, 'admin', 'admin@mail.com', md5('Admin123'));
