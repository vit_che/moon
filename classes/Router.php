<?php

class Router
{

	private $routes;

	public function __construct()
	{
		$routesPath = ROOT.'/config/routes.php';
		$this->routes = include($routesPath);
	}


	private function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
		return trim($_SERVER['REQUEST_URI'], '/');
		}
	}

	public function run()
	{
		$uri = $this->getURI();

//		var_dump($uri);
//		die();
        $uri = mb_stristr($uri, '/');
        $uri = mb_substr($uri, 1);
//        var_dump($uri);
//        die();



		foreach ($this->routes as $uriPattern => $path) {

			if(preg_match("~$uriPattern~", $uri)) {

                /* Get internal path*/
				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);
//				var_dump($internalRoute);

				$segments = explode('/', $internalRoute);

				$controllerName = array_shift($segments).'Controller';
				$controllerName = ucfirst($controllerName);

				$actionName = 'action'.ucfirst(array_shift($segments));
//                var_dump($controllerName);///

                $parameters = $segments;
//                var_dump($parameters);///

				$controllerPath = ROOT . '/controllers/' .$controllerName. '.php';
//				var_dump($controllerPath);///
				if (file_exists($controllerPath)) {
					include_once($controllerPath);
				}

				$controllerObject = new $controllerName;
				$result = call_user_func_array(array($controllerObject, $actionName), $parameters);
				
				if ($result != null) {
					break;
				}
			}
		}
	}
}
