<?php


class Validation
{
    public static function checkUserName($name){

        $db = Db::getConnection();
        $query = $db->prepare("SELECT * FROM users WHERE `name` = ? ");
        $query->execute(array($name));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $user = $query->fetch();

        if ($user) {

            return [
                "result" => false,
                "status" => "warning",
                "message" => "That username already exists! Please try another one!"
            ];
        } else {

            return [
                "result" => true,
            ];
        }
    }

    public static function checkUserPassword($password){

        $db = Db::getConnection();
        $query = $db->prepare("SELECT * FROM users WHERE `password` = ? ");
        $query->execute(array(md5($password)));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $user = $query->fetch();

        if ($user) {

            return [
                "result" => true,
            ];

        } else {

            return  [
                "result" => false,
                "status" => "danger",
                "message" => "Password is not correct!"
            ];
        }
    }


    public static function checkEditUserName($name, $old_name){

        $db = Db::getConnection();
        $query = $db->prepare("SELECT * FROM users WHERE `name` = ? AND `name` != ?");
        $query->execute(array($name, $old_name));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $user = $query->fetch();

        if ($user) {

            return [
                "result" => false,
                "status" => "warning",
                "message" => "That username already exists! Please try another one!"
            ];
        } else {

            return [
                "result" => true,
            ];
        }
    }

    public static function checkUserNameAndPassword($name, $password){

        $db = Db::getConnection();
        $query = $db->prepare("SELECT * FROM users WHERE `name` = ? ");
        $query->execute(array($name));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $userName = $query->fetch();

        $messages = [];

        if (!$userName) {
            $message = [
                "result" => false,
                "status" => "warning",
                "message" => "User is not registered!"
            ];
            $messages[] = $message;
        }

        if ($userName && $userName['password'] != md5($password)) {
            $message = [
                "result" => false,
                "status" => "danger",
                "message" => "Password is not correct!"
            ];
            $messages[] = $message;
        }

        if (count($messages) > 0){

            return [
                "result" => false,
                "messages" => $messages
            ];
        } else {

            return [ "result" => true ];
        }
    }

    public static function checkUserEmail($email){

        $db = Db::getConnection();
        $query = $db->prepare("SELECT * FROM users WHERE `email` = ? ");
        $query->execute(array($email));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $user_email = $query->fetch();

        if ($user_email) {

            return [
                "result" => false,
                "status" => "warning",
                "message" => "This email already exists! Please try another one!"
            ];
        } else {

            return [
                "result" => true,
            ];
        }
    }

    public static function checkEditUserEmail($email, $old_email){

        $db = Db::getConnection();
        $query = $db->prepare("SELECT * FROM users WHERE `email` = ? AND `email` != ?");
        $query->execute(array($email, $old_email));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $user_email = $query->fetch();

        if ($user_email) {

            return [
                "result" => false,
                "status" => "warning",
                "message" => "This email already exists! Please try another one!"
            ];
        } else {

            return [
                "result" => true,
            ];
        }
    }

    public static function clear_input($data) {

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);

        return $data;
    }

    public static function checkShortLengh($string, $lengh){

        return  (strlen($string) < $lengh) ? true : false ;
    }

    public static function checkLongLengh($string, $lengh){

        return  (strlen($string) > $lengh) ? true : false ;
    }


    public static function validateName($name){
        /** name must has 3-30 lat symbols */
        $messages = [];
        $name_max_lengh = 30;
        $name_min_lengh = 3;
        $name_req = "/[а-яА-Я!@#$%^&*()\"\'_=+.,;:\d]/";

        if (preg_match($name_req, $name)){
            $messages[] = [
                "status" => "danger",
                "message" => "The name contains unacceptable symbols"
            ];
        }

        if (Validation::checkShortLengh($name, $name_min_lengh)){
            $messages[] = [
                "status" => "danger",
                "message" => "The name less then ".$name_min_lengh." symbols"
            ];
        }

        if (Validation::checkLongLengh($name, $name_max_lengh)){
            $messages[] = [
                "status" => "danger",
                "message" => "The name more then ".$name_max_lengh." symbols"
            ];
        }

        return $messages;
    }


    public static function validateEmail($email){

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){

            return [
                "status" => "danger",
                "message" => "The email is not correct!"
            ];
        }
    }


    public static function validatePassword($password){

        if (!preg_match('/^\S*(?=\S{8,25})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/', $password)){

            return [
                "status" => "danger",
                "message" => "The password does not meet the requirements"
            ];
        }
    }


    public static function validateImage($image){

        $img_types = [ 'png', 'gif', 'jpg', 'jpeg'];
        $img_size = 128000;

        $image_size = $image['image']['size'];
        $image_ext = pathinfo($image['image']['name'], PATHINFO_EXTENSION);

        $messages = [];

        if (!in_array($image_ext, $img_types)){
            $messages[] = [
                "result" => false,
                "status" => "warning",
                "message" => "Wrong file type"
            ];
        }
        if ( $image_size > $img_size){
            $messages[] = [
                "result" => false,
                "status" => "warning",
                "message" => "To large file"
            ];
        }

        return $messages;
    }
}
