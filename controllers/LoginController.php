<?php

include_once ROOT. '/models/User.php';
require_once (ROOT.'/vendor/autoload.php');
require_once (ROOT.'/controllers/UsersController.php');


class LoginController
{

    public function actionIndex(){

        session_start();
        $messages = NULL;

        if(isset($_SESSION["session_username"])){
            $user = User::getUserByName($_SESSION["session_username"]);
            if($user){
                header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$user['id']);
                exit();
            }
        }

        if(isset($_SESSION["session_messages"])){
            $messages = $_SESSION['session_messages'];
            unset($_SESSION['session_messages']);
        }

        $loader = new \Twig\Loader\FilesystemLoader('views');
        $twig = new \Twig\Environment($loader);
        $template = $twig->load('login.html');

        echo $template->render([ 'messages' => $messages]);

        return true;
    }


    public function actionEnter(){

        if(isset($_POST) && count($_POST)>0 ) {

            if (!empty($_POST['name']) && !empty($_POST['password'])) {

                $name= Validation::clear_input($_POST['name']);
                $password= Validation::clear_input($_POST['password']);

                $check_creds_result = Validation::checkUserNameAndPassword($name, $password);

                if ( $check_creds_result['result']){

                    session_start();
                    $_SESSION['session_username'] = $name;
                    $user = User::getUserByName($name);
                    header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/users/".$user['id']);
                    exit();

                } else {
                    session_start();
                    $_SESSION['session_messages'] =  $check_creds_result['messages'];
                    header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/login");
                    exit();
                }

            } else {

                session_start();
                $messages[] = [
                    "status" => "warning",
                    "message" => "All fields required"
                ];

                $_SESSION['session_messages'] = $messages;
                header("Location: http://" . $_SERVER['HTTP_HOST']."/".SITE."/login");
                exit();
            }
        }
    }


    public function actionOut(){

        session_start();
        unset($_SESSION['session_username']);
        session_destroy();
        header("Location: http://".$_SERVER['HTTP_HOST']."/".SITE."/login");
        exit();
    }
}
