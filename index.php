<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));
define('SITE','moon');
require_once(ROOT.'/classes/Router.php');
require_once(ROOT.'/classes/Db.php');

$router = new Router();
$router->run();

