if ( document.forms['form'] != undefined ){

    var username = document.forms['form']['name'];
    var email = document.forms['form']['email'];
    var password = '';

    if (document.forms['form']['password'] != undefined){
        password = document.forms['form']['password'];
    } else {
        password = document.forms['form']['new_password'];
    }

    var name_error = document.getElementById("name error");
    var email_error = document.getElementById("email error");
    var password_error = document.getElementById("password error");

    username.addEventListener("blur", nameVerify, true );

    if (email != undefined){
        email.addEventListener("blur", emailVerify, true );
    }

    password.addEventListener("blur", passwordVerify, true );

    function Validate() {
        // name validation
        if(username.value === "") {
            username.style.border = " 1px solid red";
            name_error.textContent = 'name is required';
            username.focus();
            return false;
        }

        if (email != undefined) {
            //email validation
            if (email.value === "") {
                email.style.border = " 1px solid red";
                email_error.textContent = 'email is required';
                email.focus();
                return false;
            }
        }

        //password validation
        if(password.value === "") {
            password.style.border = " 1px solid red";
            password_error.textContent = 'Password is required';
            password.focus();
            return false;
        }

    }

    function nameVerify() {
        if ((username.value !== "") && (/^[a-zA-Z]{3,30}$/
            .test(username.value))) {
            username.style.border = "2px solid #5a948c";
            name_error.innerHTML = "";
            return true;
        } else {
            username.style.border = " 1px solid red";
            name_error.textContent = 'Name must contain ONLY  LAT LETTERS and lengh 3-30';
            username.focus();
            return false;
        }
    }

    function emailVerify() {
        if ((email.value !== "") &&
            (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                .test(email.value)))
        {
            email.style.border = "2px solid #5a948c";
            email_error.innerHTML = "";
            return true;
        } else {
            email.style.border = " 2px solid red";
            email_error.textContent = 'Please, Enter correct Email';
            email.focus();
            return false;
        }
    }

    function passwordVerify() {
        if ((password.value !== "") && ( /^\S*(?=\S{8,25})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/
            .test(password.value))) {
            password.style.border = "2px solid #5a948c";
            password_error.innerHTML = "";
            return true;
        } else {
            password.style.border = " 1px solid red";
            password_error.textContent = 'Password must be 8 to 30 characters long, contain latin letters and at least one uppercase letter and one number';
            password.focus();
            return false;
        }
    }
}
